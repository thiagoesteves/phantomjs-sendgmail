# phantomjs-sendgmail
This program sends email by gmail

# How to use?
You must change the sender user and password at sendgmail.js. After that you can end calling in the line command:

Usage:

> phantomjs sendgmail.js <destination> <subject> <text> [attach]

Ex: phantomjs sendgmail myfriend@bla.com "Dinner" "I see you there" "myphoto.jpg"

 <destination> : Destination email
 <subject>     : Subject to insert in the Subject of the email
 <text>        : What you will write in the body of the email
 <attach>      : File to attach if necessary
