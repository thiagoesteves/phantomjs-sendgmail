//  Usage:
//
// phantomjs sendgmail.js <destination> <subject> <text> [path/to/the/file]
//
//
var page = require("webpage").create();
var system = require('system');
var fs = require('fs');
var args = require('system').args;

var homePage    = "https://mail.google.com/?ui=html";
// Your account information
var gmail_user  = "user@gmail.com";
var gmail_pwd   = "password";

var dest_mail;
var dest_subj;
var dest_text;
var dest_path_file;

"use strict";
page.settings.userAgent = 'SpecialAgent';

phantom.injectJs("http://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js");

page.open(homePage, function (status) {
  // Validation
  if ((args.length != 4) && (args.length != 5)) {
    console.log('Usage: ');
    console.log('phantom sendgmail.js <destination> <subject> <text> [path/to/the/file]');
    console.log(' ');
    phantom.exit();
  } else {
    dest_mail = system.args[1];
    dest_subj = system.args[2];
    dest_text = system.args[3];
    if (args.length === 5) {
      dest_path_file = system.args[4];
    }
  }

  WaitPage();
  page.evaluate(function (user_) {
    document.getElementById("Email").value = user_;
    document.getElementById("gaia_loginform").submit();
  }, gmail_user );
  
  WaitPage();
  page.evaluate(function (pass_) {
    document.getElementById("Passwd").value = pass_;
    document.getElementById("gaia_loginform").submit();
  }, gmail_pwd );
     
  WaitPage();
  
  // Select Send Mail 
  SendTab(19);
  SendEnter();

  WaitPage();
  
  // If necessary we attach a file
  if (args.length === 5) {
    page.uploadFile('input[name="file0"]', dest_path_file);
    WaitPage();
  }
  
  // Fill the information and send
  page.evaluate(function (mail, subject, text) {
    document.getElementById("to").value = mail;
    document.querySelector('input[name="subject"]').value = subject;
    document.querySelector('textarea[name="body"]').textContent = text;
    // Click Send
    document.querySelector('input[name="nvp_bu_send"]').click();
  }, dest_mail, dest_subj, dest_text);
  
  WaitPage();
  phantom.exit();
});

function SendTab(times) {
  for (g=0; g < times; g++){ 
	page.sendEvent( 'keypress', 16777217 );
  }
}
function SendEnter() {
  page.sendEvent( 'keypress', 16777221 );
}

function WaitPage() {
  do { phantom.page.sendEvent('mousemove'); } while (page.loading);
}
